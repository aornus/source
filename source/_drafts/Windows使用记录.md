---
title: Windows使用记录
date: 2022-11-20 20:24:30
update: 2022-11-20 20:24:30
abbrlink: 304
tags:
- 操作系统
categories:
- 学习
cover:
katex: true
comments:
copyright:
aside: 
password:
hidden:
description: 
sticky: 
keywords:
root: ../../
---

## 休眠与睡眠的区别
> 休眠"是一种主要为笔记本电脑设计的电源节能状态。 睡眠通常会将工作和设置保存在内存中并消耗少量的电量，而休眠则将打开的文档和程序保存到硬盘中，然后关闭计算机。 在Windows 使用的所有节能状态中，休眠使用的电量最少。

如果这种说法是正确的话，那么在Windows电脑进入休眠状态后，如果我重启到另外一个系统，玩了一会儿又重新进Windows，那么他应该还会保存之前的工作状态。
- [x] 有待测试 2022-11-20 20:28:40
