---
title: {{title}}
date: {{date:YYYY-MM-DD HH:mm:ss}}
update: {{date:YYYY-MM-DD HH:mm:ss}}
abbrlink: sands00
tags:

categories:
- 生活
- 积累
cover: /images/sands.svg
katex: true
comments:
copyright:
aside: 
password:
hidden:
description: 【聚沙成塔·九九九】 
sticky: 
keywords:
root: ../../
---

> <center>溱与洧，方涣涣兮。士与女，方秉蕳兮</center>
> <p align="right">——《郑风·溱洧》</p>
