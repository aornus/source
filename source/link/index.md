---
title: MessageBoard📨
mathjax: true
date: 2021-09-13 21:59:16
updated:
description: 友联页面，欢迎大家来交换友链。
keywords:
top_img: 
aside: false
---

## 欢迎来到留言板📨，请留下你的足迹👣。


{% flink %}
- class_name: 友情链接
  class_desc: 🎵找啊找啊找朋友，找到许多好朋友~
  link_list:
    - name: 始终 
      link: https://liam.page/ 
      avatar: https://liam.page/images/avatar/avatar.webp
      descr: 不忘初心(Latex技术博客)


- class_name: 网站
  class_desc: 值得推荐的网站
  link_list:
  

    - name: Hexo
      link: https://hexo.io/zh-tw/
      avatar: https://d33wubrfki0l68.cloudfront.net/6657ba50e702d84afb32fe846bed54fba1a77add/827ae/logo.svg
      descr: 快速、简单且强大的网誌框架。
    - name: 大人学
      link: https://www.darencademy.com/
      avatar: https://www-image-cdn.darencademy.com/img/logo.png
      descr: 相信思考，用于改变。
    - name: 译学馆
      link: https://www.yxgapp.com/
      avatar: https://cdn-www.yxgapp.com/wp-content/uploads/2019/07/yxg_logo_small.png
      descr: 知识无疆界！
    - name: Stefan Trost Media
      link: https://www.sttmedia.com/
      avatar: https://www.sttmedia.com/favicon.ico
      descr: Software and Web Solutions（超级工具集合）
    - name: 不死鸟
      link: https://dalao.ru/
      avatar: https://npm.elemecdn.com/niaosu/dalao/vzDtmMFg.png
      descr: 资源聚合搜索
      

{% endflink %}

友链填写规范：
```yml
name: Foundation #博客名称
link: https://si-on.top #博客地址
avatar: https://cdn.jsdelivr.net/gh/aornus/blogimg/2022hong-03.png #头像地址
deacr: Sion'Blog #简短的介绍自己的博客或座右铭
#sample
- name: 
      link: https://dalao.ru/
      avatar: https://npm.elemecdn.com/niaosu/dalao/vzDtmMFg.png
      descr: 不死鸟·资源聚合搜索
```

<script async src="https://comments.app/js/widget.js?3" data-comments-app-website="suVzREf8" data-limit="5" data-color="E22F38" data-dislikes="1" data-outlined="1" data-colorful="1"></script>
    
